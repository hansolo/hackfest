/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package global.hackfest;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * User: hansolo
 * Date: 12.10.13
 * Time: 12:30
 */
public class Main extends Application {
    private Label label;

    @Override public void init() {
        label = new Label("HackFest");
    }

    @Override public void start(Stage stage) {
        StackPane pane = new StackPane();
        pane.setPrefSize(400, 400);
        pane.getStyleClass().add("hackfest");
        pane.getChildren().add(label);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(Main.class.getResource("hackfest.css").toExternalForm());

        stage.setTitle("HackFest");
        stage.setScene(scene);
        stage.show();
    }

    @Override public void stop() {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
