package com.hackfest;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import com.hackfest.communication.BackOfficeCommunicationHandler;
import com.hackfest.data.BackofficeRuntime;
import com.hackfest.ui.BackOfficeView;

public class BackOffice extends Application {

	private BackofficeRuntime runtime;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//TODO init
		BackOfficeCommunicationHandler communicationHandler = new BackOfficeCommunicationHandler();
		BackOfficeView view = new BackOfficeView();
		runtime = new BackofficeRuntime(view, communicationHandler);
		
		Scene myScene = new Scene(view);
		myScene.getStylesheets().add(BackOfficeView.class.getResource("backoffice.css").toExternalForm());
		primaryStage.setScene(myScene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
