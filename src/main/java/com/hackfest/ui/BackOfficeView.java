package com.hackfest.ui;

import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class BackOfficeView extends StackPane {

	public BackOfficeView() {
        getStyleClass().add("backoffice-main-view");
        Label label = new Label("BackOffice");
        getChildren().add(label);
	}
}
